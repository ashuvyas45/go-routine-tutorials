package main

// unbuffered channel

func main() {
	ch := make(chan int, 3)

	ch <- 1
}

// func main() {
// 	ch := make(chan int, 3)

// 	for i := 0; i < 5; i++ {
// 		ch <- 1
// 	}
// }
