package main

import (
	"fmt"
	"sync"
	"time"
)

// Understand wait group

const MaxGoRoutineToWaitFor = 0

func main() {

	var wg sync.WaitGroup

	wg.Add(MaxGoRoutineToWaitFor)

	go welcomeText(&wg)
	go welcomeTextTwo(&wg)

	wg.Wait()
	fmt.Println("Done")
}

func welcomeText(wg *sync.WaitGroup) {
	time.Sleep(1 * time.Second)
	fmt.Println("This is awesom tutorial")
	wg.Done()
}

func welcomeTextTwo(wg *sync.WaitGroup) {
	fmt.Println("This is awesom tutorial two")
	wg.Done()
}
