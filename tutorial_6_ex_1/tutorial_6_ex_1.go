package main

import "fmt"

func main() {

	ch := make(chan int, 6)
	go pushInChannel(ch)
	printFromChannel(ch)
}

func pushInChannel(ch chan int) {
	for i := 0; i < 5; i++ {
		ch <- i
	}
}

func printFromChannel(ch chan int) {
	for i := 0; i < 5; i++ {
		fmt.Println(<-ch)
	}
}
