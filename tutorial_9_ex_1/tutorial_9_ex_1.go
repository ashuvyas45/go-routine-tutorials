package main

import (
	"fmt"
	"time"
)

// real example non blocking channels

func main() {

	ch := make(chan int)
	waitDuration := time.Duration(2) * time.Second

	val, status := tryReceive(ch, waitDuration)
	if status {
		fmt.Printf("value is %v\n", val)
	} else {
		fmt.Println("we overcome the blocking channel")
	}
}

func tryReceive(ch chan int, duration time.Duration) (int, bool) {
	select {
	case val := <-ch:
		return val, true
	case <-time.After(duration):
		return 0, false
	}
}
