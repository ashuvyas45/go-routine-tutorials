package main

import "fmt"

// non blocking channels

func main() {

	ch := make(chan int)
	val, status := tryReceive(ch)
	if status {
		fmt.Printf("value is %v\n", val)
	} else {
		fmt.Println("we overcome the blocking channel")
	}
}

func tryReceive(ch chan int) (int, bool) {
	select {
	case val := <-ch:
		return val, true
	default:
		return 0, false
	}
}
