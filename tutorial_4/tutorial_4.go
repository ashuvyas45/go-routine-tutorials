package main

import "fmt"

// Channel type and value

func main() {
	ch := make(chan int)
	fmt.Printf("type of ch is %T\n", ch)
	fmt.Printf("value of ch is %v\n", ch)
}
