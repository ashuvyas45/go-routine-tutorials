package main

import (
	"fmt"
	"time"
)

// select

func main() {
	chOne := make(chan string)
	chTwo := make(chan string)

	go waitForOneSec(chOne)
	go waitForTwoSec(chTwo)

	select {
	case template := <-chOne:
		fmt.Println(template)
	case template := <-chTwo:
		fmt.Println(template)
	}
}

func waitForOneSec(ch chan string) {
	time.Sleep(1 * time.Second)
	template := fmt.Sprintf("waiting for 1 sec")
	ch <- template
}

func waitForTwoSec(ch chan string) {
	time.Sleep(2 * time.Second)
	template := fmt.Sprintf("waiting for 2 sec")
	ch <- template
}
