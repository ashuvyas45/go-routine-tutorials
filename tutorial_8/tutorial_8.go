package main

import (
	"fmt"
	"math/rand"
	"time"
)

// select use case api and timeout

func main() {
	rand.Seed(time.Now().UnixNano())
	chOne := make(chan string)
	chTwo := make(chan string)

	go apiCall(chOne)
	go timeOut(chTwo)

	select {
	case template := <-chOne:
		fmt.Println(template)
	case template := <-chTwo:
		fmt.Println(template)
	}
}

func apiCall(ch chan string) {
	responseTime := rand.Intn(5)
	time.Sleep(time.Duration(responseTime) * time.Second)
	template := fmt.Sprintf("api success: 200 with response time %v", responseTime)
	ch <- template
}

func timeOut(ch chan string) {
	time.Sleep(3 * time.Second)
	template := fmt.Sprintf("server error: timeout")
	ch <- template
}
