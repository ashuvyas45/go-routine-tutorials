package main

import (
	"fmt"
	"time"
)

// Understanding of go routine

func main() {
	go welcomeText()
	time.Sleep(1 * time.Second)
	fmt.Println("Done")
}

func welcomeText() {
	fmt.Println("This is awesom tutorial")
}
