package main

// Blocking nature of channels

func main() {
	ch := make(chan int)

	ch <- 1
}

// func main() {
// 	ch := make(chan int)

// 	val := <-ch
// 	fmt.Println(val)
// }
