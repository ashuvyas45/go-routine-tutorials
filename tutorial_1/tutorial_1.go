package main

import "fmt"

// Amaze of go routine

func main() {
	go welcomeText()
	fmt.Println("Done")
}

func welcomeText() {
	fmt.Println("This is awesom tutorial")
}
