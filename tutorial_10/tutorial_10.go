package main

import (
	"fmt"
	"time"
)

// fanout 

const channelOne = "channelOne"
const channelTwo = "channelTwo"

func main() {

	inCh := make(chan int, 10)
	ch := make(chan int)
	ch2 := make(chan int)

	putDataInChannel(inCh)

	go getChannelData(ch, channelOne)
	go getChannelData(ch2, channelTwo)

	fanOut(inCh, ch, ch2)

	time.Sleep(time.Duration(1) * time.Second)

}

func fanOut(inChan chan int, outChanOne, outChanTwo chan int) {

	for data := range inChan {
		select {
		case outChanOne <- data:
		case outChanTwo <- data:
		}
	}
}

func getChannelData(ch chan int, identifier string) {
	time.Sleep(50 * time.Microsecond)
	for value := range ch {
		fmt.Printf("data Print by channel %v and value %v\n", identifier, value)
	}
}

func putDataInChannel(ch chan int) {
	for i := 0; i < 10; i++ {
		ch <- 1
	}
	close(ch)
}

// func putDataInChannel(ch chan<- int, duration time.Duration, identifier string) {
// 	time.Sleep(duration)
// 	if identifier == channelOne {
// 		ch <- 1
// 	} else {
// 		ch <- 2
// 	}
// }
