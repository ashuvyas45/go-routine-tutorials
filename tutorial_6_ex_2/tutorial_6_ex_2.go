package main

import "fmt"

// closing channels

func main() {
	ch := make(chan int)

	close(ch)

	val, status := <-ch
	val2, status2 := <-ch
	fmt.Println(val, status)
	fmt.Println(val2, status2)
}

// func main() {
// 	ch := make(chan int)

// 	close(ch)

// 	ch <- 1
// }
